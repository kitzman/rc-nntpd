install:V:
	mkdir -p /rc/bin/rc-nntpd/handlers
	cp rc-nntpd /rc/bin/rc-nntpd/
	cp select-handler /rc/bin/rc-nntpd/
	dircp handlers /rc/bin/rc-nntpd/handlers
	mkdir -p /rc/bin/rc-nntpd/skel/^(bin rc dev storage env tmp)
